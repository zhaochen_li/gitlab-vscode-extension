import { browser } from '@wdio/globals';
import { completeAuth, verifyDuoChatResponse } from '../helpers/index.js';

describe('GitLab Workflow Extension Duo Chat', async () => {
  let workbench;

  before(async () => {
    await completeAuth();
  });

  beforeEach(async () => {
    workbench = await browser.getWorkbench();
    await workbench.executeCommand('GitLab Duo Chat: Start a new conversation');
    const duoChatWebView = await workbench.getWebviewByTitle('GitLab Duo Chat');

    await duoChatWebView.open();
  });

  it('sends a Duo chat request and receives a response', async () => {
    const duoChatInput = 'hi';
    const expectedOutput = 'GitLab Duo Chat';

    await browser.keys(duoChatInput.split());
    await browser.keys('Enter');

    await verifyDuoChatResponse(expectedOutput);
  });
});
