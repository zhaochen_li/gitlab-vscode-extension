import { DependencyContainer } from '../common/dependency_container';
import { WebIDEExtension } from '../common/platform/web_ide';
import { createGitLabPlatformManagerBrowser } from './gitlab_platform_browser';
import { GitLabTelemetryEnvironmentBrowser } from './gitlab_telemetry_environment_browser';

export const createDependencyContainer = async (
  webIdeExtension: WebIDEExtension,
): Promise<DependencyContainer> => ({
  gitLabPlatformManager: await createGitLabPlatformManagerBrowser(webIdeExtension),
  gitLabTelemetryEnvironment: new GitLabTelemetryEnvironmentBrowser(webIdeExtension),
});
