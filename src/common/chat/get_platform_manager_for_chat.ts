import { GitLabPlatformManager, GitLabPlatformForAccount } from '../platform/gitlab_platform';
import { getChatSupport } from './api/get_chat_support';

export const GITLAB_COM_URL: string = 'https://gitlab.com';
export const GITLAB_STAGING_URL: string = 'https://staging.gitlab.com';
export const GITLAB_ORG_URL: string = 'https://dev.gitlab.org';
export const GITLAB_DEVELOPMENT_URL: string = 'http://localhost';

export enum GitLabEnvironment {
  GITLAB_COM = 'production',
  GITLAB_STAGING = 'staging',
  GITLAB_ORG = 'org',
  GITLAB_DEVELOPMENT = 'development',
  GITLAB_SELF_MANAGED = 'self-managed',
}

export class GitLabPlatformManagerForChat {
  readonly #platformManager: GitLabPlatformManager;

  constructor(platformManager: GitLabPlatformManager) {
    this.#platformManager = platformManager;
  }

  async getProjectGqlId(): Promise<string | undefined> {
    const projectManager = await this.#platformManager.getForActiveProject(false);
    return projectManager?.project.gqlId;
  }

  /**
   * Obtains a GitLab Platform to send API requests to the GitLab API
   * for the Duo Chat feature.
   *
   * - It returns a GitLabPlatformForAccount for the first linked account.
   * - It returns undefined if there are no accounts linked
   */
  async getGitLabPlatform(): Promise<GitLabPlatformForAccount | undefined> {
    const platforms = await this.#platformManager.getForAllAccounts();

    if (!platforms.length) {
      return undefined;
    }

    let platform: GitLabPlatformForAccount | undefined;

    // Using a for await loop in this context because we want to stop
    // evaluating accounts as soon as we find one with code suggestions enabled
    for await (const result of platforms.map(getChatSupport)) {
      if (result.hasSupportForChat) {
        platform = result.platform;
        break;
      }
    }

    return platform;
  }

  async getGitLabEnvironment(): Promise<GitLabEnvironment> {
    const platform = await this.getGitLabPlatform();
    const instanceUrl = platform?.account.instanceUrl;

    switch (instanceUrl) {
      case GITLAB_COM_URL:
        return GitLabEnvironment.GITLAB_COM;
      case GITLAB_DEVELOPMENT_URL:
        return GitLabEnvironment.GITLAB_DEVELOPMENT;
      case GITLAB_STAGING_URL:
        return GitLabEnvironment.GITLAB_STAGING;
      case GITLAB_ORG_URL:
        return GitLabEnvironment.GITLAB_ORG;
      default:
        return GitLabEnvironment.GITLAB_SELF_MANAGED;
    }
  }
}
