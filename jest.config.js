// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  preset: 'ts-jest',
  clearMocks: true,
  coverageProvider: 'v8',
  coverageReporters: ['cobertura'],
  coverageDirectory: './reports',
  roots: ['src'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: 'reports',
        outputName: 'unit.xml',
        titleTemplate: '{title}',
        classNameTemplate: '{classname}',
      },
    ],
  ],
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/webviews/', '/dist-/'],
  transformIgnorePatterns: ['/node_modules/(?!(@anycable/core|nanoevents)/)'],
  transform: {
    '/node_modules/(@anycable/core|nanoevents).+\\.(js|ts)$': 'ts-jest',
  },
  testEnvironment: 'node',
};
